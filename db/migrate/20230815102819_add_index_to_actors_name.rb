class AddIndexToActorsName < ActiveRecord::Migration[7.0]
  def change
    add_index :actors, :name
  end
end
