class AddAverageRatingToMovies < ActiveRecord::Migration[7.0]
  def change
    add_column :movies, :average_rating, :decimal, precision: 3, scale: 2
    add_index :movies, :average_rating
  end
end
