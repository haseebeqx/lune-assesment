class UpdateAverageRatingJob < ApplicationJob
  queue_as :default

  def perform(movie_id)
    movie = Movie.find(movie_id)
    movie.update_average_rating!
  end
end
