class ReviewsImporter < BaseCsvImporter
  def import_rows(rows)
    rows.each do |row|
      movie = Movie.find_by!(title: row['Movie'])
      user = User.find_or_create_by!(name: row['User'])
      Review.create!(movie: movie, user: user, stars: row['Stars'], review_text: row['Review'])
    end
  end
end
