class BaseCsvImporter
  BATCH_SIZE = 1000
  attr_reader :file_path, :logger

  def initialize(file_path, log_file_name)
    @file_path = file_path
    @logger = Logger.new(log_file_name)
  end

  def import
    logger.info "Starting import from #{file_path}..."
    CSV.foreach(file_path, headers: true, encoding: 'utf-8').each_slice(BATCH_SIZE) do |rows|
      ActiveRecord::Base.transaction do
        begin
          import_rows(rows)
        rescue => e
          logger.error "Error importing row: #{e.message}"
          raise ActiveRecord::Rollback
        end
      end
    end
    logger.info "Import completed successfully."
  end

  def import_rows(rows)
    raise NotImplementedError, "Subclasses must implement import_rows method"
  end
end
