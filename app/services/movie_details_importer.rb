class MovieDetailsImporter < BaseCsvImporter
  def import_rows(rows)
    rows.each do |row|
      movie = Movie.find_or_create_by!(title: row['Movie'], description: row['Description'], year: row['Year'], director: row['Director'], country: row['Country'])
      actor = Actor.find_or_create_by!(name: row['Actor'])
      location = Location.find_or_create_by!(filming_location: row['Filming location'])
      
      unless movie.actors.exists?(actor.id)
        movie.actors << actor
      end

      unless movie.locations.exists?(location.id)
        movie.locations << location
      end
    end
  end
end
