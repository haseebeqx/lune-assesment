class MoviesController < ApplicationController
  def index
    @movies = Movie.order(average_rating: :desc, title: :asc).page(params[:page]).per(50)
  end

  def show
    @movie = Movie.find(params[:id])
  end
end
