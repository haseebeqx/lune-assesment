class ActorsController < ApplicationController
  def index
    if params[:search]
      @actors = Actor.where("name LIKE ?", "%#{params[:search]}%").page(params[:page]).per(50)
    else
      @actors = Actor.page(params[:page]).per(50)
    end
  end

  def show
    @actor = Actor.find(params[:id])
  end
end
