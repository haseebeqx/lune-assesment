class Review < ApplicationRecord
  belongs_to :movie
  belongs_to :user

  after_save :update_movie_average_rating
  after_destroy :update_movie_average_rating


  def update_movie_average_rating
    UpdateAverageRatingJob.perform_later(movie.id)
  end
end
