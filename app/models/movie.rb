class Movie < ApplicationRecord
  has_many :movie_actors
  has_many :actors, through: :movie_actors
  has_many :movie_locations
  has_many :locations, through: :movie_locations
  has_many :reviews
  has_many :users, through: :reviews

  def update_average_rating!
    avg_rating = reviews.average(:stars)
    update_column(:average_rating, avg_rating)
  end
end
