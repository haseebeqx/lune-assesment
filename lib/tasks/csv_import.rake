namespace :csv_import do
  desc "Import movie details"
  task movie_details: :environment do
    file_path = Rails.root.join('movies.csv')
    MovieDetailsImporter.new(file_path, 'log/movie_details_import.log').import
  end

  desc "Import reviews"
  task reviews: :environment do
    file_path = Rails.root.join('reviews.csv')
    ReviewsImporter.new(file_path, 'log/reviews_import.log').import
  end
end
