# README

## Setup

```
bundle install
rake db:migrate
```

## Rake tasks

```
rake csv_import:movie_details # to import movie details
rake csv_import:reviews # to import reviews
```

## Running

```
bin/dev
```

## Possible improvements

* use gems like `activerecord-import` to perform bulk inserts more efficiently
* use full text search using postgresql or elasticsearch
* Could have added a better `gin` index to actors if postgresql was used.

